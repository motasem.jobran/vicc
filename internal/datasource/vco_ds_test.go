package datasource

import (
	"VCO/config"
	"os"
	"testing"
)

// initDataSourceConfig initialize configuration for test issue
func initDataSourceConfig() *config.DataSourceConfiguration {
	return &config.DataSourceConfiguration{
		BaseUrl:   "test",
		BasicUser: "test",
		BasicPass: "test",
	}
}

func TestVcoDataSourceImpl_GetAllCarLinesFromVICI(t *testing.T) {
	os.Clearenv()
	ds := NewVcoDataSource(initDataSourceConfig())
	_, _ = ds.GetAllCarLinesFromVICI("test")
}
