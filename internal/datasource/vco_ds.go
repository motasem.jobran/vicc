package datasource

import (
	"VCO/config"
	"VCO/internal/entities"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// VcoDataSourceImpl implements the VcoDataSource interface
type VcoDataSourceImpl struct {
	config *config.DataSourceConfiguration
}

// NewVcoDataSource factory for the datasource
func NewVcoDataSource(configuration *config.DataSourceConfiguration) *VcoDataSourceImpl {
	return &VcoDataSourceImpl{configuration}
}

// GetAllCarLines gets all car line for specific tenant
func (ds *VcoDataSourceImpl) GetAllCarLinesFromVICI(tenant string) (entities.CarLines, error) {
	url := ds.config.BaseUrl + "catalogue/carlines?tenant=" + tenant + "&fetchPrices=false&fetchMedia=false&addErrorMps=false"
	result, _ := doRequest(url, *ds.config)

	var carLines entities.CarLines
	err := json.Unmarshal(result, &carLines)
	if err != nil {
		return entities.CarLines{}, err
	}

	return carLines, nil
}

func (ds *VcoDataSourceImpl) GetVehicleCatalogSummaryFromVICI(tenant string, carlineCode string) (entities.OverView, entities.SalesGroups, []entities.Models, error) {
	// get overview
	overview := getOverView(*ds.config, tenant, carlineCode)
	salesGroups, err := getSalesGroups(tenant, carlineCode, *ds.config)
	if err != nil {
		return entities.OverView{}, entities.SalesGroups{}, []entities.Models{}, err
	}

	var allModels []entities.Models
	for _, salesGroup := range salesGroups.SalesGroups {
		models := getAllModels(tenant, carlineCode, salesGroup.Code, *ds.config)
		allModels = append(allModels, models)
	}

	return overview, salesGroups, allModels, nil
}

// getOverView gets the name and code of carline
func getOverView(config config.DataSourceConfiguration, tenant string, carlineCode string) entities.OverView {
	url := config.BaseUrl + "/catalogue/overview?tenant=" + tenant + "&carlineKey=" + carlineCode +
		"&fetchPrices=true&fetchMedia=false&fetchMandatory=true&fetchTechnical=true&fetchEco=true&fetchWltp=true&addErrorMps=false"
	result, _ := doRequest(url, config)

	var overview entities.OverView
	err := json.Unmarshal(result, &overview)
	if err != nil {
		return entities.OverView{}
	}
	return overview
}

// getSalesGroups gets all sales groups for a given tenant and carline code
func getSalesGroups(tenant string, carlineCode string, config config.DataSourceConfiguration) (entities.SalesGroups, error) {
	url := config.BaseUrl + "catalogue/salesgroups?tenant=" + tenant + "&carlineKey=" + carlineCode +
		"&fetchPrices=false&fetchMedia=false&addErrorMps=false"
	result, _ := doRequest(url, config)

	var salesGroups entities.SalesGroups
	err := json.Unmarshal(result, &salesGroups)
	if err != nil {
		return entities.SalesGroups{}, err
	}

	return salesGroups, nil
}

// getAllModels helper function to get all models for specific sales group
func getAllModels(tenant string, carlineCode string, saleGroupCode string, config config.DataSourceConfiguration) entities.Models {
	url := config.BaseUrl + "/catalogue/models?tenant=" + tenant + "&carlineKey=" + carlineCode + "&salesgroupKey=" +
		saleGroupCode + "&fetchPrices=true&fetchMandatory=true&fetchTechnical=true&fetchEco=true&fetchWltp=true&fetchMedia=false&addErrorMps=false"
	result, _ := doRequest(url, config)

	var models entities.Models
	err := json.Unmarshal(result, &models)
	if err != nil {
		return entities.Models{}
	}

	return models
}

// doRequest helper function to create a request to the external DS
func doRequest(url string, config config.DataSourceConfiguration) ([]byte, error) {
	client := &http.Client{}
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	// read the basic auth information
	username := config.BasicPass
	password := config.BasicPass

	request.SetBasicAuth(username, password)
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	bodyText, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return bodyText, nil
}
