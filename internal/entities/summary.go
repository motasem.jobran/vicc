package entities

// Summary is a container for information to create the catalogue summary dto
type Summary struct {
	OverView    OverView
	SalesGroups SalesGroups
	Models      []Models
}
