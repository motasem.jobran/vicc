package entities

// Models list of model type
type Models struct {
	Models []Model
}

// Model type
type Model struct {
	Code    string
	Name    string
	Year    string
	Version string
}
