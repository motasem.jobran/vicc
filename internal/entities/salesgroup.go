package entities

// SalesGroups type
type SalesGroups struct {
	SalesGroups []SalesGroup
}

// SalesGroup type
type SalesGroup struct {
	Code string
	Name string
}
