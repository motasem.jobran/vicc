package entities

// CarLines list of carline type
type CarLines struct {
	CarLines []Carline
}

// Carline type
type Carline struct {
	Name string
	Code string
}
