package service

import (
	"VCO/internal/entities"
	"VCO/mock"
	"github.com/go-playground/assert/v2"
	"testing"
)

func TestVcoServiceImpl_GetAllCarLines(t *testing.T) {
	var toReturnFromDs []entities.Carline
	carline := entities.Carline{
		Name: "TestName",
		Code: "TestCode",
	}
	toReturnFromDs = append(toReturnFromDs, carline)

	dataSourceMock := mock.NewVcoDataSourceMock()
	service := NewVcoService(dataSourceMock)
	dataSourceMock.On("GetAllCarLinesFromVICI").Return(entities.CarLines{CarLines: toReturnFromDs})
	result := service.GetAllCarLines("test")
	assert.Equal(t, result, entities.CarLines{CarLines: toReturnFromDs})

	dataSourceMock.AssertNumberOfCalls(t, "GetAllCarLinesFromVICI", 1)
}

func TestVcoServiceImpl_GetVehicleCatalogSummary(t *testing.T) {
	dataSourceMock := mock.NewVcoDataSourceMock()
	service := NewVcoService(dataSourceMock)
	dataSourceMock.On("GetVehicleCatalogSummaryFromVICI").Return(entities.OverView{}, entities.SalesGroups{}, []entities.Models{}, nil)
	service.GetVehicleCatalogSummary("test", "code")

	dataSourceMock.AssertNumberOfCalls(t, "GetVehicleCatalogSummaryFromVICI", 1)
}
