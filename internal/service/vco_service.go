package service

import (
	"VCO/internal/entities"
)

// VcoDataSource interface for the datasource
type VcoDataSource interface {
	GetAllCarLinesFromVICI(tenant string) (entities.CarLines, error)
	GetVehicleCatalogSummaryFromVICI(tenant string, carlineCode string) (entities.OverView, entities.SalesGroups, []entities.Models, error)
}

type VcoServiceImpl struct {
	DataSource VcoDataSource
}

// NewVcoService factory for the Vco service service
func NewVcoService(ds VcoDataSource) *VcoServiceImpl {
	return &VcoServiceImpl{ds}
}

// GetAllCarLines gets all CarLines for a given tenant
func (vcoService *VcoServiceImpl) GetAllCarLines(tenant string) entities.CarLines {
	res, _ := vcoService.DataSource.GetAllCarLinesFromVICI(tenant)
	return res
}

// GetVehicleCatalogSummary gets a summary of vehicle for a given tenant and carline
func (vcoService *VcoServiceImpl) GetVehicleCatalogSummary(tenant string, carlineCode string) (entities.OverView, entities.SalesGroups, []entities.Models) {
	overview, salesGroup, allModels, _ := vcoService.DataSource.GetVehicleCatalogSummaryFromVICI(tenant, carlineCode)
	return overview, salesGroup, allModels
}
