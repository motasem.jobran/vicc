package config

import (
	"fmt"
	"github.com/joho/godotenv"
	"os"
)

// Load loads the env vars
func Load(fileName string, folderName string) error {
	err := godotenv.Load(folderName + fileName)
	if err != nil && fileExists(fileName) {
		return err
	}

	return nil
}

// fileExists helper method to check if a file exists or not
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}

	return !info.IsDir()
}

// Getenv gets env var value
func Getenv(varName string) string {
	return os.Getenv(varName)
}

// MustGetenv gets env var value, will panic if var is not set
func MustGetenv(varName string) string {
	value := os.Getenv(varName)
	if value == "" {
		panic(fmt.Sprintf("Can't get ENV variable `%s`", varName))
	}

	return value
}
