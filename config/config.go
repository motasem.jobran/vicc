package config

// This file will load the configuration required for the application

type DataSourceConfiguration struct {
	BaseUrl   string
	BasicUser string
	BasicPass string
}

func ReadConfig() (*DataSourceConfiguration, error) {
	return &DataSourceConfiguration{
		BaseUrl:   MustGetenv("BASE_URL"),
		BasicUser: MustGetenv("BASIC_USER"),
		BasicPass: MustGetenv("BASIC_PWD"),
	}, nil
}
