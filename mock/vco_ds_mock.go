package mock

import (
	"VCO/internal/entities"
	"github.com/stretchr/testify/mock"
)

// VcoDataSourceMock type
type VcoDataSourceMock struct {
	mock.Mock
}

// NewVcoDataSourceMock factory for VcoDataSourceMock
func NewVcoDataSourceMock() *VcoDataSourceMock {
	return &VcoDataSourceMock{}
}

func (mock *VcoDataSourceMock) GetAllCarLinesFromVICI(tenant string) (entities.CarLines, error) {
	args := mock.Called()
	return args.Get(0).(entities.CarLines), nil
}

func (mock *VcoDataSourceMock) GetVehicleCatalogSummaryFromVICI(tenant string, carlineCode string) (entities.OverView, entities.SalesGroups, []entities.Models, error) {
	mock.Called()
	return entities.OverView{}, entities.SalesGroups{}, []entities.Models{}, nil
}
