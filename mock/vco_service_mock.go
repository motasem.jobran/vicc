package mock

import (
	"VCO/internal/entities"
	"github.com/stretchr/testify/mock"
)

type VCOServiceMock struct {
	mock.Mock
}

func NewVCOServiceMock() *VCOServiceMock {
	return &VCOServiceMock{}
}

func (mock *VCOServiceMock) GetAllCarLines(tenant string) entities.CarLines {
	args := mock.Called()
	return args.Get(0).(entities.CarLines)
}

func (mock *VCOServiceMock) GetVehicleCatalogSummary(tenant string, carlineCode string) (entities.OverView, entities.SalesGroups, []entities.Models) {
	args := mock.Called()
	return args.Get(0).(entities.OverView), args.Get(1).(entities.SalesGroups), args.Get(2).([]entities.Models)
}
