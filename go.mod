module VCO

go 1.13

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/assert/v2 v2.0.1
	github.com/joho/godotenv v1.3.0
	github.com/lytics/logrus v0.0.0-20170528191427-4389a17ed024
	github.com/stretchr/testify v1.5.1
	golang.org/x/sys v0.0.0-20200523222454-059865788121 // indirect
)
