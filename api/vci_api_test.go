package api

import (
	"VCO/internal/entities"
	"VCO/mock"
	"bytes"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"net/http"
	"net/http/httptest"
	"testing"
)

func PerformRequest(handler http.Handler, method, path string, body []byte) *httptest.ResponseRecorder {
	request, _ := http.NewRequest(method, path, bytes.NewBuffer(body))

	recorder := httptest.NewRecorder()
	handler.ServeHTTP(recorder, request)
	return recorder
}

// initCarline helper function to initialize a test carline
func initCarline() entities.Carline {
	return entities.Carline{
		Name: "TestName",
		Code: "TestCode",
	}
}

func TestVCOApi_GetAllCarLines(t *testing.T) {
	// Arrange the service and router
	vcoServiceMock := mock.NewVCOServiceMock()
	router := gin.Default()
	InjectApiHandlers(vcoServiceMock, router)

	t.Run("FAILS WHEN the tenant is NOT GIVEN", func(t *testing.T) {
		// Act
		result := PerformRequest(router, "GET", "/v1/tenant//carlines", nil)

		// Assert
		assert.Equal(t, http.StatusBadRequest, result.Code)
		vcoServiceMock.AssertNumberOfCalls(t, "GetAllCarLines", 0)
	})

	t.Run("RETURNS the list of CarLines WHEN the tenant is GIVEN", func(t *testing.T) {
		// Arrange
		carLine := initCarline()
		var carLinesList []entities.Carline
		for idx := 0; idx < 2; idx++ {
			carLinesList = append(carLinesList, carLine)
		}
		list := entities.CarLines{CarLines: carLinesList}
		vcoServiceMock.On("GetAllCarLines").Return(list)
		expectedResult := `[{"name":"TestName","code":"TestCode"},{"name":"TestName","code":"TestCode"}]`

		// Act
		result := PerformRequest(router, "GET", "/v1/tenant/test/carlines", nil)

		// Assert
		assert.Equal(t, http.StatusOK, result.Code)
		assert.Equal(t, expectedResult, result.Body.String())
		vcoServiceMock.AssertNumberOfCalls(t, "GetAllCarLines", 1)
	})
}

func TestVCOApi_GetVehicleCatalogSummary(t *testing.T) {
	// Arrange the service and router
	vcoServiceMock := mock.NewVCOServiceMock()
	router := gin.Default()
	InjectApiHandlers(vcoServiceMock, router)

	t.Run("FAILS WHEN the tenant is NOT GIVEN", func(t *testing.T) {
		// Act
		result := PerformRequest(router, "GET", "/v1/tenant//catalog?carline=12345", nil)

		// Assert
		assert.Equal(t, http.StatusBadRequest, result.Code)
		vcoServiceMock.AssertNumberOfCalls(t, "GetVehicleCatalogSummary", 0)
	})

	t.Run("FAILS WHEN the carline is NOT GIVEN", func(t *testing.T) {
		// Act
		result := PerformRequest(router, "GET", "/v1/tenant/test/catalog", nil)

		// Assert
		assert.Equal(t, http.StatusBadRequest, result.Code)
		vcoServiceMock.AssertNumberOfCalls(t, "GetVehicleCatalogSummary", 0)
	})

	t.Run("RETURNS summary of carLines WHEN the tenant and carline are GIVEN", func(t *testing.T) {
		// Arrange
		carLine := initCarline()
		overview := entities.OverView{Carline: carLine}

		salesGroup := entities.SalesGroup{
			Code: "TestCodeGroup",
			Name: "TestNameGroup",
		}
		var salesGroupList []entities.SalesGroup
		salesGroupList = append(salesGroupList, salesGroup)
		salesGroups := entities.SalesGroups{SalesGroups: salesGroupList}

		model := entities.Model{
			Code:    "TestModelCode",
			Name:    "TestModelName",
			Year:    "TestModelYear",
			Version: "TestModelVersion",
		}
		var listOfModels []entities.Model
		listOfModels = append(listOfModels, model)
		allModels := entities.Models{Models: listOfModels}

		var listOfAllModels []entities.Models
		listOfAllModels = append(listOfAllModels, allModels)
		vcoServiceMock.On("GetVehicleCatalogSummary").Return(overview, salesGroups, listOfAllModels)
		router := gin.Default()
		InjectApiHandlers(vcoServiceMock, router)
		expectedResult := `{"name":"TestName","code":"TestCode","salesGroups":[{"name":"TestNameGroup","code":"TestCodeGroup","models":[{"name":"TestModelName","code":"TestModelCode","version":"TestModelVersion","modelYear":"TestModelYear"}]}]}`

		// Act
		result := PerformRequest(router, "GET", "/v1/tenant/test/catalog?carline=12345", nil)

		// Assert
		assert.Equal(t, http.StatusOK, result.Code)
		assert.Equal(t, expectedResult, result.Body.String())
		vcoServiceMock.AssertNumberOfCalls(t, "GetVehicleCatalogSummary", 1)
	})
}
