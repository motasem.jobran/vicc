package dto

// CatalogueSummaryDTO type
type CatalogueSummaryDto struct {
	Name        string           `json:"name"`
	Code        string           `json:"code"`
	SalesGroups []SalesGroupsDto `json:"salesGroups"`
}

// SalesGroupsDto type
type SalesGroupsDto struct {
	Name   string     `json:"name"`
	Code   string     `json:"code"`
	Models []ModelDto `json:"models"`
}

// ModelDto type
type ModelDto struct {
	Name      string `json:"name"`
	Code      string `json:"code"`
	Version   string `json:"version"`
	ModelYear string `json:"modelYear"`
}
