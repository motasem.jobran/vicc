package dto

type CarlineDto struct {
	Name string `json:"name"`
	Code string `json:"code"`
}
