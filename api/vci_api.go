package api

import (
	"VCO/api/mapper"
	"VCO/internal/entities"
	"github.com/gin-gonic/gin"
	"net/http"
)

// The service interface that will be used in this api will be her defined.
// But, the real implementation will be in the service

// VcoService interface
type VcoService interface {
	GetAllCarLines(tenant string) entities.CarLines
	GetVehicleCatalogSummary(tenant string, carlineCode string) (entities.OverView, entities.SalesGroups, []entities.Models)
}

// VcoApi type
type VcoApi struct {
	// add the field required for this type e.g service
	service VcoService
}

// NewVCIApi factory for the VCIApi
func NewVcoApi(service VcoService) *VcoApi {
	return &VcoApi{service}
}

// handler to get all CarLines for a given tenant
func (api *VcoApi) GetAllCarLines(ctx *gin.Context) {
	tenant := ctx.Param("tenant")
	if tenant == "" {
		ctx.JSON(http.StatusBadRequest, "tenant is not given")
		return
	}

	res := api.service.GetAllCarLines(tenant)
	if len(res.CarLines) == 0 {
		ctx.JSON(http.StatusBadRequest, "can't find the given tenant")
		return
	}

	ctx.JSON(http.StatusOK, mapper.ToCarLinesDto(res.CarLines))
	return
}

// GetVehicleCatalogSummary handler to get a summary for specific carline
func (api *VcoApi) GetVehicleCatalogSummary(ctx *gin.Context) {
	tenant := ctx.Param("tenant")
	if tenant == "" {
		ctx.JSON(http.StatusBadRequest, "tenant is not given")
		return
	}
	//carlineCode := ctx.Param("carline")
	carlineCode := ctx.Request.URL.Query().Get("carline")
	if carlineCode == "" {
		ctx.JSON(http.StatusBadRequest, "carline not given")
		return
	}
	overview, res, allModels := api.service.GetVehicleCatalogSummary(tenant, carlineCode)
	if len(res.SalesGroups) == 0 {
		ctx.JSON(http.StatusBadRequest, "can't find the given tenant")
		return
	}
	ctx.JSON(http.StatusOK, mapper.ToCatalogueSummaryDTO(overview, res, allModels))
	return
}

// InjectApiHandlers injects vci handlers into the application
func InjectApiHandlers(service VcoService, route *gin.Engine) {
	api := NewVcoApi(service)

	v1 := route.Group("/v1")
	{
		v1.GET("/tenant/:tenant/carlines", api.GetAllCarLines)
		v1.GET("/tenant/:tenant/catalog", api.GetVehicleCatalogSummary)
	}
}
