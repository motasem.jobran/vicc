package mapper

import (
	"VCO/api/dto"
	"VCO/internal/entities"
)

// ToCarlineDto converts an carline to a carline dto
func ToCarlineDto(carline entities.Carline) dto.CarlineDto {
	return dto.CarlineDto{
		Name: carline.Name,
		Code: carline.Code,
	}
}

// ToCarLinesDto converts list of entities to a list of carline dto
func ToCarLinesDto(carLines []entities.Carline) []dto.CarlineDto {
	var carlineDTOs []dto.CarlineDto
	for _, carline := range carLines {
		carlineDTOs = append(carlineDTOs, ToCarlineDto(carline))
	}

	return carlineDTOs
}
