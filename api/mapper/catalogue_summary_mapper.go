package mapper

import (
	"VCO/api/dto"
	"VCO/internal/entities"
)

// ToCatalogueSummaryDTO converts
func ToCatalogueSummaryDTO(overview entities.OverView, salesGroups entities.SalesGroups, models []entities.Models) dto.CatalogueSummaryDto {
	var catalogueSummary dto.CatalogueSummaryDto
	catalogueSummary.Name = overview.Carline.Name
	catalogueSummary.Code = overview.Carline.Code
	catalogueSummary.SalesGroups = ToSalesGroupsDto(salesGroups, models)

	return catalogueSummary
}

// ToSalesGroups converts list of sales groups into dto of sales groups
func ToSalesGroupsDto(groups entities.SalesGroups, models []entities.Models) []dto.SalesGroupsDto {
	var salesGroupsDto []dto.SalesGroupsDto
	for idx, group := range groups.SalesGroups {
		var temp dto.SalesGroupsDto
		temp.Name = group.Name
		temp.Code = group.Code
		temp.Models = ToModelsDto(models[idx])
		salesGroupsDto = append(salesGroupsDto, temp)
	}

	return salesGroupsDto
}

// ToModelsDto converts list of models into list of dto of model
func ToModelsDto(models entities.Models) []dto.ModelDto {
	var modelsDto []dto.ModelDto
	for _, model := range models.Models {
		var temp dto.ModelDto
		temp.Name = model.Name
		temp.Code = model.Code
		temp.ModelYear = model.Year
		temp.Version = model.Version
		modelsDto = append(modelsDto, temp)
	}

	return modelsDto
}
