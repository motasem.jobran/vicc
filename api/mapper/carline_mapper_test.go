package mapper

import (
	"VCO/api/dto"
	"VCO/internal/entities"
	"testing"
)

// initCarlineEntity helper function to initialize a carline entity
func initCarlineEntity() entities.Carline {
	carline := entities.Carline{
		Name: "TestName",
		Code: "TestCode",
	}

	return carline
}

// initCarlineDto helper function to initialize a carline dto
func initCarlineDto() dto.CarlineDto {
	carLineDto := dto.CarlineDto{
		Name: "TestName",
		Code: "TestCode",
	}

	return carLineDto
}

// TestToCarlineDto tests convert an entity to dto of carline
func TestToCarlineDto(t *testing.T) {
	carline := initCarlineEntity()
	expectedCarLineDto := initCarlineDto()

	carlineDto := ToCarlineDto(carline)
	if carlineDto != expectedCarLineDto {
		t.Errorf("objects were not correctly mapped: expected %v \n get %v \n", expectedCarLineDto, carlineDto)
	}
}

// TestToCarLinesDto tests the conversion an array of carline into dto of carline
func TestToCarLinesDto(t *testing.T) {
	var carLines []entities.Carline
	for idx := 0; idx < 3; idx++ {
		carLines = append(carLines, initCarlineEntity())
	}

	var expectedCarLinesDto []dto.CarlineDto
	for idx := 0; idx < 3; idx++ {
		expectedCarLinesDto = append(expectedCarLinesDto, initCarlineDto())
	}

	result := ToCarLinesDto(carLines)
	if expectedCarLinesDto != nil {
		for idx := range result {
			if result[idx] != expectedCarLinesDto[idx] {
				t.Errorf("objects were not correctly mapped: expected %v \n get %v \n", expectedCarLinesDto, result)
			}
		}
	}
}
