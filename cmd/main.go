package main

import (
	"VCO/api"
	"VCO/config"
	"VCO/internal/datasource"
	"VCO/internal/service"
	"github.com/gin-gonic/gin"
	"log"
)

func main() {
	// define the place of the configuration folder
	configFolder := "./config/"
	err := config.Load(".env", configFolder)
	if err != nil {
		log.Fatal("Error loading .env file", err)
	}
	datasourceConfig, err := config.ReadConfig()
	if err != nil {
		log.Fatal("Error loading datasource configuration")
	}
	router := gin.Default()

	dataSource := datasource.NewVcoDataSource(datasourceConfig)
	vcoService := service.NewVcoService(dataSource)
	api.InjectApiHandlers(vcoService, router)

	if err := router.Run("localhost:8080"); err != nil {
		panic(err)
	}
}
